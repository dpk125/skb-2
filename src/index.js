import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2A', (req, res) => {
  let sum = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(sum.toString());
});

app.get('/task2B', (req, res) => {
  let fullname = (req.query.fullname || '').trim();

  if (!isValidName(fullname)) {
    return res.send('Invalid fullname');
  }

  let words = fullname
    .toLowerCase()
    .split(/\s+/)
    .map(word => word[0].toUpperCase() + word.substring(1));

  let name = words.pop();

  let result = words.map(word => word[0] + '.');
  result.unshift(name);

  res.send(result.join(' '));
});

app.get('/task2C', (req, res) => {
  let url = req.query.username || '';
  let username = url.match(/\b\/@?([\w\.]+)|^@?([\w\.]+)$/);

  if (username === null) {
    return res.send('Invalid username');
  }

  res.send(formatUsername(username[1] || username[2]));
});

const responses = [
  1,
  18,
  243,
  3240,
  43254,
  577368,
  7706988,
  102876480,
  1373243544,
  18330699168,
  244686773808,
  3266193870720,
  43598688377184,
  581975750199168,
  7768485393179328,
  103697388221736960,
  1384201395738071424,
  18476969736848122368,
  246639261965462754048,
];

app.get('/task2X', (req, res) => {
  let n = req.query.i;

  res.send(responses[n].toString());
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});

const isValidName = name => /^(([^\x00-\x7F]|[a-z'])+\s+){0,2}([^\x00-\x7F]|[a-z'])+$/i.test(name);
const formatUsername = username => '@' + username;
